package danyleon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.out.println(" ");
        System.out.println("*****************************************************************************************************************");
        System.out.println("Escribir una función que realice la suma de dos números. No se deberá usar (+) o ningún operador aritmético.");
        System.out.println("*****************************************************************************************************************");
        System.out.println(" ");

        /** Establecemos números a sumar */
        Double n1 = 20.0;
        Double n2 = 45.8;

        /** Llamamos método para sumar */
        Double sum = sum(n1, n2);

        System.out.println("Primer número a sumar: " + n1);
        System.out.println("Segundo número a sumar: " + n2);
        System.out.println("------------------------------");
        System.out.println("Resultado de la suma: " + sum);
    }

    /**
     * @param x
     * @param y
     */
    private static Double sum(Double x, Double y) {
        /** Agregamos los números a sumar a una lista */
        List<Double> numbers = new ArrayList<>();

        numbers.add(x);
        numbers.add(y);

        /** Realizamos la suma */
        return numbers.stream().mapToDouble(n -> n).sum();
    }
}
